#!/bin/bash

x=$1
touch outputFinal${x}.txt

# [length of language] - most guesses are 30-50, opponents say it's much higher- up to several thousand or more.
for lang_len in $1 #35 40 45 50 55 60
do
# [% unset parameters in language] - likely no more than 1-2%. - set to 0% to 2%
for percent_unset_lang in 0 5 10 15 20 #0 5 10 15 20
do
# [# grammars] - smaller is significantly better, but we need to check for large ones.
for num_grammars in 20 40 60 80 100
do
# [average # parameters set by each sentence] a range between 3-30, to start, weighted towards the smaller numbers.
for average_num_unset in 3 6 9 12 #{3..30..9}
do
# [# recombination points, where 0=no recombination] 0..5
for recomb_points in 0 5 10 15 20
do
# [mutation probability] 0.01..0.1 1-10%
for mutation_prob in 0 1 3 5 7 9 10 #{1..10..3}
do
# [survivor selection method, where -g is generational, -a is age-based, -w is replace worst, & -e is elitism]
for surv_select in "-g" "-a" "-w" "-e"
do
# [survivor count, # to replace for age/worst or # to save for elitism]
for surv_count in 1 5 10 15 20 #{1..10..3}
do
# [termination values, # of rounds steady] - should be high if we're going through a large number of sentences
for term_val in 500 #{500..500}
do
# [exciting - bilingual / negative reinforcement / polar opposite / chance incorrect]
for exciting in 0 "-b" "-n" "-p" 1 4 7 10 
do
p_unset_lang=`echo "$percent_unset_lang/100" | bc -l`
mutation_p=`echo "$mutation_prob/100" | bc -l`
echo -ne "$lang_len\t$p_unset_lang\t$num_grammars\t$average_num_unset\t$recomb_points\t$mutation_p\t$surv_select\t$surv_count\t$term_val\t$exciting\t" >> outputFinal${x}.txt
POS_MIN_ROUNDS=`ruby acquire_language.rb\
				$lang_len\
				$p_unset_lang\
				$num_grammars\
				$average_num_unset\
				$recomb_points\
				$mutation_p\
				$surv_select\
				$surv_count\
				$term_val\
				$exciting`
echo "$POS_MIN_ROUNDS" >> outputFinal${x}.txt
done #10
done #9
done #8
done #7
done #6
done #5
done #4
done #3
done #2
done #1
