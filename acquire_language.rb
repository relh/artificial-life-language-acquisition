#!/usr/bin/ruby
# -*- coding: iso-8859-1 -*-

#uses ruby 1.8.5

#In our genetic algorithm, which will be implemented in a scripting language, agents will be grammars, each
#grammar being a set of parameters. Our genotypes will be initially random bitstrings with parameters set to 0 or 1.
#This models the idea that children store parameters rather than sentences as they develop. During each round, a
#schema will be generated that sets some parameters to the value of the language?s grammar. All unset parameters
#will be represented by *. This schema represents an input sentence which has already been lexed and parsed into its
#parameters. Different parameters will be set in the input schema based probabilistically to model how different
#parameters appear at different frequencies in a language. The number of parameters which can be set in a schema
#will be determined by an average parameter density value. The phenotype will be the ability of an agent?s grammar
#to match the input grammar. The fitness function will be the Hamming distance between the agent?s grammar and the
#input grammar. This model?s a child?s ability to match the parameters set in his or her grammar to those set by a
#sentence the child hears. After the fitness is calculated, the algorithm will use Roulette wheel selection to determine
#which grammars will be used in reproduction; these grammars will be recombined and mutated with set
#probabilities, then a new generation of agents will be produced using full replacement. The algorithm will terminate
#when the best-scoring grammar bit-string remains the same over a set number of generations, and success of an
#algorithm run can be measured by the best Hamming distance between an agent?s grammar and the language?s
#grammar.

#variable defaults
#$lang_len = 30
#$one_cutoff = 0.5
#$unset_cutoff = 1.0
#$pop_size = 30
#$set_cutoff = 6.0/$lang_len # whether the parameter will be left unset or taken from the language
#$num_recomb_pts = 5
#$pm = 0.01
#$survivor_select_method = "-g"
#$survivor_select_count = 20 # doesn't matter when -g
#$termination_num = 500

#check for correct # of inputs; we assume that the inputs are of the correct format
if ARGV.length != 10
	puts 'Parameter usage: acquire_language.rb
		[length of language]
		[% unset parameters in language]
		[# grammars]
		[average # parameters set by each sentence]
		[# recombination points, where 0=no recombination]
		[mutation probability]
		[survivor selection method, where -g is generational, -a is age-based, -w is replace worst, & -e is elitism]
		[survivor count, # to replace for age/worst or # to save for elitism]
		[termination condition: # rounds]
		[bilingual / negative reinforcement / polar opposite languages / % chance incorrectly set sentence parameter]'
	puts 'failed: acquire_language.rb 30 0.0 30 6 5 0.01 -g 20 500 0 0 0 0.0'
	exit
else
	$lang_len = ARGV[0].to_i
	$one_cutoff = (1-ARGV[1].to_f)/2
	$unset_cutoff = 1-ARGV[1].to_f
	$pop_size = ARGV[2].to_i
	$set_cutoff = ARGV[3].to_f/$lang_len # whether the parameter will be left unset or taken from the language
	$num_recomb_pts = ARGV[4].to_i
	$pm = ARGV[5].to_f
	$survivor_select_method = ARGV[6]
	$survivor_select_count = ARGV[7].to_i
	$termination_num = ARGV[8].to_i
	$exciting = ARGV[9]
	if ($exciting == "-b" || $exciting == "-n" || $exciting == "-p")
		$chance_incorrect = 0.0
	else
		$exciting == "-i"
		$chance_incorrect = ARGV[9].to_f/100
	end	 
end #EMILY: added input [survivor count, # to replace for age/worst or # to save for elitism]

$language = ""
$bilang = ""

#array methods
class Array
  def shuffle
    sort_by { rand }
  end
  def sample
    self[rand(length)]
  end
end

#generate a canonical language grammar
def make_canonical_grammar
	lang = ""
	for i in 0..$lang_len-1
		i = rand()
		if i<$one_cutoff 
			lang.concat("0")
		elsif (i<$unset_cutoff)
			lang.concat("1")
		else
			lang.concat("*")
		end
	end
	lang
end

$language = make_canonical_grammar
if ($exciting == "-b")
	$bilang = make_canonical_grammar
elsif ($exciting == "-p") #if so, assemble polar opposite grammar
	$bilang = ""
	for i in 0..$lang_len-1
		if ($language[i] == "1")
			$bilang.concat("0")
		else
			$bilang.concat("1")
		end
	end
end

#initialize genotypes
$grammars = []
for j in 0..$pop_size-1
	$grammars[j] = ""
	for i in 0..$lang_len-1
		#select either 0 or 1 randomly
 		i = rand(2)
 		$grammars[j].concat(i.to_s)
	end
end

#---------------------meta variables---------------------------
$termination = false
$rounds = 0

$best_grammar = 0
$odd_grammar = 0
$even_grammar = 0

#---------------------make sentence schema---------------------------
#A schema is a sentence used to test our grammars.
def make_schema(lang)
	sentence = ""
	for i in 0..$lang_len-1
		j = rand()
		#set parameters incorrectly with a certain probability to model speech error
		k = rand();
		if k<($chance_incorrect*1.0/$lang_len)
			sentence.concat(((lang[i].to_i-1).abs).to_s)
		elsif j<$set_cutoff
			($exciting == "-n") ? sentence.concat(((lang[i].to_i-1).abs).to_s) : sentence.concat(lang[i])
		else
			sentence.concat("*")
		end
	end
	sentence
end

#---------------------calculate fitness---------------------------
#our fitness function, an inverse Hamming distance calculator.
def hamming(grammar, sentence)
	score = 0
	for i in 0..grammar.length-1
		if sentence[i] == "*"[0] || grammar[i] == sentence[i]
			score += 1
		end
	end
	score
end

#our negative fitness function, a tool where if our grammar doesn't match a sentence, it gets basically a 0 score
def negHamming(grammar, sentence)
	score = 0
	for i in 0..grammar.length-1
		if sentence[i] == "*"[0] || grammar[i] != sentence[i]
			score += 1 # score will increase unless you match the sentence
		else
			score = 1 # has to be greater than 0
			break # if we match, fitness is ~0: we're scolded
		end
	end
	score
end

#---------------------mutate child grammars---------------------------
def mutate child
	#children aren't as long as the language
	mutate_pts = []
	for j in 0..($pm*$lang_len).round-1
		i = rand($lang_len)
		while mutate_pts.include? i
			i = rand($lang_len)
		end #make sure that we get (pm*lang_len).round unique points
		mutate_pts[j] = i
		if child[i]=="0"
 			child[i]="1"
 		else
 			child[i]="0"
 		end
	end
	child
end

#---------------------recombine parents---------------------------
def recombine parents
	k = 0
	child_grammars = []
	while k < parents.length - 1
		parent_one = parents[k]
		parent_two = parents[k+1]
		
		#recombine
		recomb_pts = []
		recomb_pts[0] = 0
		for j in 1..$num_recomb_pts
			i = rand($lang_len)
			while recomb_pts.include? i
				i = rand($lang_len)
			end #make sure that we get num_recomb_pts unique points
			recomb_pts[j] = i
		end
		
		recomb_pts[$num_recomb_pts+1] = $lang_len
		recomb_pts.sort! #0..[recombination points in order]...lang_len
 		child_one = ""
 		child_two = ""
 		for j in 0...$num_recomb_pts+1
 			if j%2==1  #alternate taking from parent 1 or 2
 				child_one += parent_one[recomb_pts[j]..recomb_pts[j+1]-1]
 				child_two += parent_two[recomb_pts[j]..recomb_pts[j+1]-1]
 			else
 				child_two += parent_one[recomb_pts[j]..recomb_pts[j+1]-1]
 				child_one += parent_two[recomb_pts[j]..recomb_pts[j+1]-1]
 			end
 		end
 		child_grammars[k] = child_one
 		child_grammars[k+1] = child_two
		k+=2
	end
	if k!=parents.length
		child_grammars[k] = parents[k]
	end #if odd number of parents, copy over last one with no recombination
	child_grammars
end

#---------------------reproduction---------------------------
#For every pair of parents, recombine them to produce children, mutate the children, and store them in the grammars list
#basic testing complete
def reproduce parents	
	#The Array#sample method doesn't exist in 1.8.5.
	parents_randomly_paired = parents.shuffle #rearranges parents
	#so they are paired randomly when used in following loop
	child_grammars = recombine parents_randomly_paired
	for i in 0..child_grammars.length-1
		child_grammars[i] = mutate child_grammars[i]
	end
	child_grammars
end

#---------------------rounds start here---------------------------
#main loop - each iteration is another generation.
while !$termination
	#create a sentence schema from the language
	if ($exciting == "-b" || $exciting == "-p")
		sentence = ($rounds%2 == 1) ? make_schema($bilang) : make_schema($language)
	else
		sentence = make_schema($language)
	end

	#fitness = grammar length - hamming distance, aim to maximize
	#also, create wheel array such that each individual's slice of the wheel is proportional to its fitness
	fitness = []
	wheel = []
	total_fitness = 0.0
	for j in 0..$pop_size-1
		fitness[j] = ($exciting == "-n") ? negHamming($grammars[j], sentence) : hamming($grammars[j], sentence)
		total_fitness += fitness[j]
		if j==0 
			wheel[j] = fitness[j]
		else
			wheel[j] = fitness[j] + wheel[j-1]
		end
	end

	max_fit = fitness.zip((0..fitness.length).to_a).sort #sorted array of arrays, where each array is [fitness value, original index]
	
	j = 0
	# look at only the most fit
	while j<max_fit.length && max_fit[j][0] == max_fit[0][0]
		if ($exciting == "-b" || $exciting == "-p")
			if ($rounds%2 == 1)
				compare_to_original_odd = (j==0) ? hamming($grammars[max_fit[j][1]],$bilang) : [hamming($grammars[max_fit[j][1]], $bilang), compare_to_original_odd].max
			else
				compare_to_original_even = (j==0) ? hamming($grammars[max_fit[j][1]],$language) : [hamming($grammars[max_fit[j][1]], $language), compare_to_original_even].max
			end
		else
			compare_to_original = (j==0) ? hamming($grammars[max_fit[j][1]],$language) : [hamming($grammars[max_fit[j][1]], $language), compare_to_original].max
		end
		
		j+=1
	end #of the grammars with the highest fitness score, find the highest match to the language

	if ($exciting == "-b" || $exciting == "-p")
		($rounds%2 == 1) ? ($odd_grammar = [$odd_grammar, compare_to_original_odd].max) :
							($even_grammar = [$even_grammar, compare_to_original_even].max)
	else
		$best_grammar = [$best_grammar, compare_to_original].max #find highest match over entire run
	end
	
	#parental selection using the fitness proportional selection method implemented using stochastic universal sampling
	#each selection wheel goes from 0 to total_fitness and has mu arms of length arm_space apart
	#-g is generational, -a is age-based, -w is replace worst, & -e is elitism
	parents = []
	if $survivor_select_method=="-g" 
		num_parents = $pop_size	
	elsif $survivor_select_method=="-a" || $survivor_select_method=="-w"
		num_parents = $survivor_select_count
	elsif $survivor_select_method=="-e"
		num_parents = $pop_size - $survivor_select_count
	end #EMILY: added parental selection method

	arm_space = rand(total_fitness)/(num_parents*1.0)
	
	#calling rand on 0 will return a random float between 0 and 1.
	if total_fitness == 0
		puts "0"
		exit 1
	end
	
	i = 0
	j = 0 # need help understanding selection
	while j<num_parents  #while still need to select parents
		while arm_space<=wheel[i] #while there is an arm on this individual's wheel slice #EMILY: corrected r to arm_space
			parents[j] = $grammars[i]
			arm_space += total_fitness/(num_parents*1.0)

			j += 1
		end #add individual to parent pool and advance arm
		if i==$pop_size-1 
			i=0
		else
			i+=1
		end #go to next individual, wrapping to front of wheel as needed
	end

	child_grammars = reproduce parents
	#survivor selection using: generational selection OR age-based replacement OR replace worst/no duplicates OR elitism
	#-g is generational, -a is age-based, -w is replace worst, & -e is elitism
	if $survivor_select_method=="-g" 
		$grammars = child_grammars.map {|e| e.dup} #save just the new grammars
	elsif $survivor_select_method=="-a"
		for m in 0..$survivor_select_count-1
			$grammars.unshift(child_grammars[m])
			$grammars.pop
		end #add new grammars to front of the list & remove old grammars
	elsif $survivor_select_method=="-w"
		fitness_sorted = fitness.sort
		indices = fitness.map{|e| fitness_sorted.index(e)} #get the original indices of the now sorted list
		for m in 0..$survivor_select_count-1
			$grammars.insert(indices[m], child_grammars[m]) #replace bad grammar with new grammar
		end
	elsif $survivor_select_method=="-e"
		grammars_tmp = child_grammars.map {|e| e.dup} #save the new grammars (like generational)
		fitness_sorted = fitness.sort.reverse
		indices = fitness.map{|e| fitness_sorted.index(e)} #get the original indices of the now sorted list
		for m in 0..$survivor_select_count-1
			grammars_tmp.push($grammars[indices[m]]) #tack on elite grammar unaltered
		end
		$grammars = grammars_tmp.map {|e| e.dup}
	end #EMILY: added survivor selection method
	
	#decides whether the termination condition has been met
	$rounds+=1
	#if fitness_counter>=$termination_num
	if ($rounds >= $termination_num)	
		$termination = true
	end
end

# calculate consensus sequence and print its score!
if ($exciting == "-b" || $exciting == "-p")
	consensusSeq1 = ""
	consensusSeq2 = ""
	for i in 0..$lang_len-1
		count1 = 0
		count2 = 0
		tot1 = 0
		tot2 = 0
		for j in 0..$pop_size-1
			if (hamming($grammars[j], $language) > hamming($grammars[j], $bilang))
				count1 += $grammars[j].split('')[i].to_i
				tot1 += 1
			else
				count2 += $grammars[j].split('')[i].to_i
				tot2 += 1
			end
		end
		if count1 > tot1/2
			consensusSeq1.concat("1")
		else
			consensusSeq1.concat("0")
		end
		if count2 > tot2/2
			consensusSeq2.concat("1")
		else
			consensusSeq2.concat("0")
		end
	end
	consensusScore1 = hamming(consensusSeq1, $language) # Prints score of consensus sequence
	consensusScore2 = hamming(consensusSeq2, $bilang) # Prints score of consensus sequence
	puts "#{consensusScore1*1.0/$lang_len}\t#{consensusScore2*1.0/$lang_len}\t#{$even_grammar*1.0/$lang_len}\t#{$odd_grammar*1.0/$lang_len}\t#{compare_to_original_odd*1.0/$lang_len}\t#{compare_to_original_even*1.0/$lang_len}"
else
	consensusSeq = ""
	for i in 0..$lang_len-1
		count = 0
		for j in 0..$pop_size-1
			count += $grammars[j].split('')[i].to_i
		end
		if count > $pop_size/2
			consensusSeq.concat("1")
		else
			consensusSeq.concat("0")
		end
	end
	consensusScore = hamming(consensusSeq, $language) # Prints score of consensus sequence
	puts "#{consensusScore*1.0/$lang_len}\t#{$best_grammar*1.0/$lang_len}\t#{compare_to_original*1.0/$lang_len}" #changed to print best grammar over entire run rather than best grammar at termination
end

# produce consensus sequence at end
# produce > average lang a is lang a, < average is lang b
